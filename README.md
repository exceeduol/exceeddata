# README #

The ExceedData package is designed specifically to access different types of data from the EXCEED project via databases and files.

### How do I get set up? ###

Fairly simple to do this. Install the package and then use it:

```S
devtools::install_bitbucket("exceeduol/ExceedData")
library(ExceedData)
...
```

You will need a recent version of R/RStudio to install and use the package.
To access the databases and files you will need to setup an ssh tunnel, the instructions for doing are not provided here because this is a public repository.

Currently you should install the RMySQL and devtools packages. In future these should be automatically installed when ExceedData is installed:

```S
install.packages(c("RMySQL","devtools")
```

You will need to create a 'config' object first. This is passed to many of the functions in the ExceedData package:

```S

config<-ExceedData::core.new_config(
  "dbuser",
  "dbpassword",
  "dateofdatafreeze"
)

ExceedData::primary.get_child_readcodes(config,"H3...",TRUE,"3")
```

### Contribution guidelines ###

Tests are written using the 'testthat' package. Basic logic for primary care data and spirometry data retrieval are covered.
It is recommended that when new functions are added additional tests are built to cover different use cases.